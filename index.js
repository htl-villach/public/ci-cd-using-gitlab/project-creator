import fetch from 'node-fetch';
import csv from 'csv-parser';
import fs from 'fs';

const BASE_URL = 'https://www.gitlab.com/api/v4';

async function checkToken() {
  // https://docs.gitlab.com/ee/api/users.html#user-status

  console.log('Checking validity of GITLAB_TOKEN');
  const response = await fetch(BASE_URL + '/user', {
    headers: { 'PRIVATE-TOKEN': process.env.GITLAB_TOKEN },
  });

  if (response.status == 401) {
    console.log('GITLAB_TOKEN invalid - API returns 401 Unauthorized');
    process.exit(1);
  }

  const respBody = await response.json();

  console.log(
    `GITLAB_TOKEN valid - belongs to ${respBody.name} (${respBody.username})`,
  );
}

async function checkGroup(gitlabGroup) {
  // https://docs.gitlab.com/ee/api/namespaces.html#get-namespace-by-id

  console.log('Checking existence of group ' + gitlabGroup);

  const response = await fetch(
    BASE_URL + '/namespaces/' + encodeURIComponent(gitlabGroup),
    { headers: { 'PRIVATE-TOKEN': process.env.GITLAB_TOKEN } },
  );

  if (response.status == 404) {
    console.log('group invalid - API returns 404 not found');
    process.exit(1);
  }

  const respBody = await response.json();
  console.log(
    `Group ${gitlabGroup} exists - id ${respBody.id} (${respBody.web_url})`,
  );

  return respBody.id;
}

async function getUserId(userName) {
  const response = await fetch(
    BASE_URL + '/users?username=' + encodeURIComponent(userName),
    { headers: { 'PRIVATE-TOKEN': process.env.GITLAB_TOKEN } },
  );

  if (response.status == 404) {
    return -1;
  }

  const respBody = await response.json();
  return respBody.length == 1 ? respBody[0].id : -1;
}

async function createProject(
  gitlabGroup,
  groupId,
  name,
  userNames,
  accessLevel,
) {
  let userIds = [];
  for (let userName of userNames) {
    let userId = await getUserId(userName);
    if (userId != -1) {
      userIds.push(userId);
    } else {
      console.log(`user ${userName} does not exist - skipping project ${name}`);
      return;
    }
  }

  // Not all attributes working ATM :(
  // https://gitlab.com/gitlab-org/gitlab/-/issues/335460

  const projToCreate = {
    name: name,
    emails_disabled: true,
    namespace_id: groupId,
    visibility: 'private',
    service_desk_enabled: false,
    pages_access_level: 'disabled',
    snippets_enabled: false,
    wiki_enabled: false,
    request_access_enabled: false,
    container_registry_enabled: false,
    packages_enabled: false,
    operations_access_level: 'disabled',
    analytics_access_level: 'disabled',
    requirements_access_level: 'disabled',
    initialize_with_readme: true,
  };

  console.log('Creating project ' + name);
  const response = await fetch(BASE_URL + '/projects/', {
    method: 'post',
    body: JSON.stringify(projToCreate),

    headers: {
      'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
      'Content-Type': 'application/json',
    },
  });

  const createdProject = await response.json();

  if (response.status == 400) {
    console.log('  Project already exists');
  } else if (response.status == 201) {
    console.log('  Project created with id ' + createdProject.id);

    // https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project

    const membersToAdd = {
      user_id: userIds.join(),
      access_level: accessLevel,
    };

    console.log('  Adding members to project ' + name);
    const membersResp = await fetch(
      BASE_URL + '/projects/' + createdProject.id + '/members',
      {
        method: 'post',
        body: JSON.stringify(membersToAdd),

        headers: {
          'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
          'Content-Type': 'application/json',
        },
      },
    );

    const members = await membersResp.json();
    if (membersResp.status == 201) {
      console.log('  Members successfully added');
    } else if (membersResp.status == 400 && members?.message?.access_level) {
      console.log('  Warning - some users have already inherited membership');
    } else {
      console.log('  Error adding members to project');
    }
  }

  /* 
    const projId = createdProject.id;
    createdProject.requirements_access_level = 'disabled';
    createdProject.security_and_compliance_enabled = false;
    
    const updateResponse = await fetch(BASE_URL + '/projects/' + projId, {
        method: 'put',
        body: JSON.stringify(createdProject),
        headers: { 'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
        'Content-Type': 'application/json'}});

    const updatedProject = await updateResponse.json();
    console.log(updatedProject);
    */
}

async function main() {
  console.log('Checking existance of GITLAB_TOKEN env var');

  if (!process.env.GITLAB_TOKEN) {
    console.log('GITLAB_TOKEN not set... exiting');
    process.exit(1);
  }

  let args = process.argv.slice(2);

  if (args.length < 2 || args.length > 3) {
    console.log('usage: node index.js gitlabgroup file [Maintainer|Developer]');
    process.exit(1);
  }

  const gitlabGroup = args[0];
  const inputFile = args[1];
  const desiredRole = args.length==3 ? args[2] : "Maintainer";

  const supportedRoles = { Reporter: 20, Developer: 30, Maintainer: 40 };

  if (!(desiredRole in supportedRoles)) {
    console.log(`Desired role ${desiredRole} not supported`);
    process.exit(1);
  }

  await checkToken();
  let groupId = await checkGroup(gitlabGroup);

  let projectsToCreate = [];

  fs.createReadStream(inputFile)
    .pipe(csv(['project', 'usernames']))
    .on('data', async (row) => {
      if (row.project) {
        projectsToCreate.push({
          name: row.project,
          usernames: row.usernames.trim().split(/[ ,]+/),
        });
      }
    })
    .on('end', async () => {
      console.log('CSV file successfully processed');

      for (let proj of projectsToCreate) {
        await createProject(
          gitlabGroup,
          groupId,
          proj.name,
          proj.usernames,
          supportedRoles[desiredRole],
        );
      }
    });
}

main();
