# project-creator

## Scope

Tool for automated generation of projects. Projects are created with a
minimal set of features enabled. The tool is command line tool online
and implemented using node.js.

## Getting started

Clone and install dependencies as usual...

```shell
git clone https://gitlab.com/htl-villach/internal/project-creator.git
cd project-creator
npm install
```

Prepare a CSV file like `projects.csv` with all of the projects and usernames. The first field reflects
the name of the project, the second is the username
that should get permissions on that project.

```csv
kofler,ingokofler
karasek,acka99
```

In case of multiple developers per project separate usernames with spaces

```csv
team-syp,acka99 ingokofler
```

Prepare a GitLab group that acts as the parent folder of the projects to be created,
e.g., `htl-villach/informatik/2023-4ahif/wmc`. The user that is running the script needs to have
_Owner_ permissions on that group.

Furthermore, create a personal access token for authentication. See [GitLab documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) for more details.
As usual, the personal access token needs to be passed as environment variable `GITLAB_TOKEN`.

Depending on the operating system this needs to set
before running the tool.

Powershell

```powershell
$env:GITLAB_TOKEN='fqxGMJr89mzDY3xyzWE-'
```

Linux

```shell
GITLAB_TOKEN="fqxGMJr89mzDY3xyzWE"
```

Finally, run the tool by specifying the parent GitLab group, the CSV file
and optionally the desired role. By default the role will be set to `Maintainer`
which allows for pushing to protected branches like `main`.

```shell
npm run create htl-villach/informatik/2023-4ahif/wmc projects.csv
```

If the users should get `Reporter` permissions only, one can specify that role as third parameter as follows

```shell
npm run create htl-villach/informatik/2023-4ahif/wmc projects.csv Reporter
```

## Theory of operation

* The tool will check if the `GITLAB_TOKEN` is set and belongs to a valid user.
* The tool will process the csv file line-by-line.
* First, the existence of the specified user names is checked.
* If the users exist, the project will be created and the users
  will be assigned as _Maintainer_ (the default) or _Developer_ (needs to be set explicitly) to that project.
* Success / failure will be reported to the console and
  the tool will continue with the next line.

## Example

See the file [`sample.csv`](sample.csv).
